# PHPAnalysis

#### 介绍
php使用PHPAnalysis提取关键字中文分词

#### 软件架构
php使用PHPAnalysis提取关键字中文分词

#### 安装教程

下载后解压放到extend目录下（以tp5为例，其他目录也行）

#### 使用说明

$data['seo']['keyword'] = Analysis::getKeywords($article_info['title']);


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 技术支持


1.  周涛博客 [zhoutao.org](https://www.zhoutao.org)
2.  QQ : 6045564